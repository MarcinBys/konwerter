﻿namespace Konwersja
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultTextBox = new System.Windows.Forms.TextBox();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.preinfButton = new System.Windows.Forms.RadioButton();
            this.infpreButton = new System.Windows.Forms.RadioButton();
            this.infpostButton = new System.Windows.Forms.RadioButton();
            this.postinfButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.executeButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // resultTextBox
            // 
            this.resultTextBox.Location = new System.Drawing.Point(15, 204);
            this.resultTextBox.Multiline = true;
            this.resultTextBox.Name = "resultTextBox";
            this.resultTextBox.Size = new System.Drawing.Size(430, 62);
            this.resultTextBox.TabIndex = 0;
            // 
            // inputTextBox
            // 
            this.inputTextBox.Location = new System.Drawing.Point(14, 36);
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(430, 23);
            this.inputTextBox.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.postinfButton);
            this.groupBox1.Controls.Add(this.infpostButton);
            this.groupBox1.Controls.Add(this.infpreButton);
            this.groupBox1.Controls.Add(this.preinfButton);
            this.groupBox1.Location = new System.Drawing.Point(14, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(147, 133);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Konwersja";
            // 
            // preinfButton
            // 
            this.preinfButton.AutoSize = true;
            this.preinfButton.Location = new System.Drawing.Point(8, 24);
            this.preinfButton.Name = "preinfButton";
            this.preinfButton.Size = new System.Drawing.Size(130, 19);
            this.preinfButton.TabIndex = 0;
            this.preinfButton.TabStop = true;
            this.preinfButton.Text = "Prefix -> Infix";
            this.preinfButton.UseVisualStyleBackColor = true;
            this.preinfButton.CheckedChanged += new System.EventHandler(this.preinfButton_CheckedChanged);
            // 
            // infpreButton
            // 
            this.infpreButton.AutoSize = true;
            this.infpreButton.Location = new System.Drawing.Point(8, 50);
            this.infpreButton.Name = "infpreButton";
            this.infpreButton.Size = new System.Drawing.Size(130, 19);
            this.infpreButton.TabIndex = 1;
            this.infpreButton.TabStop = true;
            this.infpreButton.Text = "Infix -> Prefix";
            this.infpreButton.UseVisualStyleBackColor = true;
            this.infpreButton.CheckedChanged += new System.EventHandler(this.infpreButton_CheckedChanged);
            // 
            // infpostButton
            // 
            this.infpostButton.AutoSize = true;
            this.infpostButton.Location = new System.Drawing.Point(8, 78);
            this.infpostButton.Name = "infpostButton";
            this.infpostButton.Size = new System.Drawing.Size(137, 19);
            this.infpostButton.TabIndex = 2;
            this.infpostButton.TabStop = true;
            this.infpostButton.Text = "Infix -> Postfix";
            this.infpostButton.UseVisualStyleBackColor = true;
            this.infpostButton.CheckedChanged += new System.EventHandler(this.infpostButton_CheckedChanged);
            // 
            // postinfButton
            // 
            this.postinfButton.AutoSize = true;
            this.postinfButton.Location = new System.Drawing.Point(8, 106);
            this.postinfButton.Name = "postinfButton";
            this.postinfButton.Size = new System.Drawing.Size(137, 19);
            this.postinfButton.TabIndex = 3;
            this.postinfButton.TabStop = true;
            this.postinfButton.Text = "Postfix -> Infix";
            this.postinfButton.UseVisualStyleBackColor = true;
            this.postinfButton.CheckedChanged += new System.EventHandler(this.postinfButton_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Wyrażenie (każdy element wyrażenia należy oddzielić spacją!)";
            // 
            // executeButton
            // 
            this.executeButton.Location = new System.Drawing.Point(190, 89);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(249, 101);
            this.executeButton.TabIndex = 4;
            this.executeButton.Text = "Wykonaj";
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(453, 277);
            this.Controls.Add(this.executeButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.inputTextBox);
            this.Controls.Add(this.resultTextBox);
            this.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Konwerter: prefix - infix - postfix";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox resultTextBox;
        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton postinfButton;
        private System.Windows.Forms.RadioButton infpostButton;
        private System.Windows.Forms.RadioButton infpreButton;
        private System.Windows.Forms.RadioButton preinfButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button executeButton;
    }
}

