﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Konwersja
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (preinfButton.Checked && !inputTextBox.Equals(null))
                {
                    resultTextBox.Text = PrefixToInfix(inputTextBox.Text);
                }
                else if (infpreButton.Checked && !inputTextBox.Equals(null))
                {
                    resultTextBox.Text = InfixToPrefix(inputTextBox.Text);
                }
                else if (infpostButton.Checked && !inputTextBox.Equals(null))
                {
                    resultTextBox.Text = InfixToPostfix(inputTextBox.Text);
                }
                else if (postinfButton.Checked && !inputTextBox.Equals(null))
                {
                    resultTextBox.Text = PostfixToInfix(inputTextBox.Text);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private int getPriority(char? oper)
        {
            switch (oper)
            {
                case '+':
                    return 0;
                case '-':
                    return 0;
                case '*':
                    return 1;
                case '/':
                    return 1;
                case '^':
                    return 2;
                default:
                    return -1;
            }
        }

        private bool IsNumber(string s)
        {
            try
            {
                int.Parse(s);
                return true;
            }
            catch (Exception e) { }
            return false;
        }

        private bool IsLetter(string s)
        {
            try
            {
                char c = char.Parse(s);

                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
                {
                    return true;
                }
            }
            catch (Exception e) { }
            return false;
        }

        public string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private string InfixToPostfix(string infix)
        {
            string[] elements = infix.Split(' ');
            StringBuilder postfix = new StringBuilder();
            Stack<char> stack = new Stack<char>();

            foreach (string ele in elements)
            {
                char c = ele[0];

                if (IsNumber(ele) || IsLetter(ele))
                {
                    postfix.Append(ele + " ");
                }
                else if (c.Equals('('))
                {
                    stack.Push(ele[0]);
                }
                else if (c.Equals(')'))
                {
                    while (!stack.Peek().Equals('('))
                        postfix.Append(stack.Pop() + " ");
                    stack.Pop();
                }
                else
                {
                    while (!stack.Count.Equals(0) && getPriority(stack.Peek()) > getPriority(c))
                        postfix.Append(stack.Pop() + " ");
                    stack.Push(c);
                }
            }

            while (!stack.Count.Equals(0))
            {
                postfix.Append(stack.Pop() + " ");
            }

            return postfix.ToString();
        }

        private string InfixToPrefix(string infix)
        {
            string[] elements = infix.Split(' ');
            //StringBuilder prefix = new StringBuilder();
            Stack<char> stackOper = new Stack<char>();
            Stack<string> stackExpr = new Stack<string>();

            foreach (string ele in elements)
            {
                char c = ele[0];

                if (IsNumber(ele) || IsLetter(ele))
                {
                    stackExpr.Push(ele + " ");
                }
                else if (c.Equals('('))
                {
                    stackOper.Push(ele[0]);
                }
                else if (c.Equals(')'))
                {
                    while (!stackOper.Peek().Equals('('))
                    {
                        char oper = stackOper.Pop();
                        string a = stackExpr.Pop();
                        string b = stackExpr.Pop();

                        stackExpr.Push(oper + " " + b + a);
                    }
                    stackOper.Pop();    //wyrzuc ze stosu '('
                }
                else
                {
                    while (!stackOper.Count.Equals(0) && getPriority(stackOper.Peek()) > getPriority(c))
                    {
                        char oper = stackOper.Pop();
                        string a = stackExpr.Pop();
                        string b = stackExpr.Pop();

                        stackExpr.Push(oper + " " + b + a);
                    }

                    stackOper.Push(c);
                }
            }

            while (!stackOper.Count.Equals(0))
            {
                char oper = stackOper.Pop();
                string a = stackExpr.Pop();
                string b = stackExpr.Pop();

                stackExpr.Push(oper + " " + b + a);
            }

            return stackExpr.Peek().ToString();
        }

        private string PrefixToInfix(string prefix)
        {
            string[] elements = Reverse(prefix).Split(' ');
            //Console.WriteLine(string.Join("", elements));
            //StringBuilder infix = new StringBuilder();
            Stack<string> stack = new Stack<string>();
            
            foreach (string ele in elements)
            {
                char c = ele[0];

                if (IsNumber(ele) || IsLetter(ele))
                {
                    stack.Push(ele + " ");
                }
                else
                {
                    string a = stack.Pop();
                    string b = stack.Pop();

                    stack.Push("(" + a + c + " " + b + ")");
                }
            }
            return stack.Peek();
        }

        private string PostfixToInfix(string postfix)
        {
            string[] elements = postfix.Split(' ');
            //Console.WriteLine(string.Join("", elements));
            //StringBuilder infix = new StringBuilder();
            Stack<string> stack = new Stack<string>();
            
            foreach (string ele in elements)
            {
                char c = ele[0];

                if (IsNumber(ele) || IsLetter(ele))
                {
                    stack.Push(ele + " ");
                }
                else
                {
                    string a = stack.Pop();
                    string b = stack.Pop();

                    stack.Push("(" + b + c + " " + a + ")");
                }
            }
            return stack.Peek();
        }


        #region Radio Buttons
        private void preinfButton_CheckedChanged(object sender, EventArgs e)
        {
            inputTextBox.Clear();
        }

        private void infpreButton_CheckedChanged(object sender, EventArgs e)
        {
            inputTextBox.Clear();
        }

        private void infpostButton_CheckedChanged(object sender, EventArgs e)
        {
            inputTextBox.Clear();
        }

        private void postinfButton_CheckedChanged(object sender, EventArgs e)
        {
            inputTextBox.Clear();
        }
        #endregion
    }
}
